# CTRL+ALT ZOMBIE ESC

# Bugs
 - [x] [B001] Sophie image not checked in - be careful not to lose!
 - [x] [B002] Midnight to 5am should be NIGHT not MORNI
 - [x] [B003] Weapons dont show in inventory
 - [x] [B004] Multi hit weapons not working
 - [x] [B005] Zombies not being attracted

# Features
 - [x] [F001] Zombie attacks
 - [x] [F002] Weapons
 - [x] [F003] Looting
 - [x] [F004] Ending 1: Find Sophie
   - [x] [F004.1] Find dogfood
   - [x] [F004.2] Find sophie in park
   - [x] [F004.3] Feed sophie dogfood
   - [x] [F004.4] Befriend sophie
   - [x] [F004.5] Make it home
   - [x] [F004.6] WIN
 - [x] [F005] Ending 2: Stockpile Food
 - [x] [F006] Ending 3: Escape To The Woods
 - [~] [F007] Ending 4: Fix the Boat
 - [x] [F008] Ending 5: Kill sewer zombie
 - [x] [F009] Forest
 - [~] [F010] Cliffs
 - [~] [F011] Castle
 - [x] [F012] Sewer + Sewer Puzzle
 - [x] [F013] Show HP in inventory
 - [x] [F014] Weapon spawns
 - [x] [F015] Level ups and more HP

# Nit
 - [x] [N001] Resting should display a message

