//
// (c) GameJammers 2021
// http://www.jamming.games
//

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const sveltePreprocess = require('svelte-preprocess')

const DIST_PATH = path.resolve(__dirname, 'public');

mode = process.env.NODE_ENV == 'production' ? 'production' : 'development'

module.exports = {

  mode,

  //
  // entry
  //
  entry: path.join(__dirname, 'src', 'index.js'),

  //
  // output
  //
  output: {
    path: DIST_PATH,
    filename: 'bundle.js'
  },

  //
  // devServer
  //
  devServer: {
    contentBase: DIST_PATH
  },

  //
  // modules
  //
  module: {
    rules: [

      // text -----------------------------------------------------------------
      {
        test: /\.ascii$/i,
        use: [ 'text-loader' ]
      },

      // svelte ---------------------------------------------------------------
      {
        test: /\.svelte$/i,
        use: {
          loader: 'svelte-loader',
          options: {
            emitCss: true,
            preprocess: sveltePreprocess({
              postcss: true
            })
          }
        }
      },
      

      // CSS ------------------------------------------------------------------
      {
        test: /\.css$/i,
        use: [ 'style-loader', 'css-loader' ]
      },

      // BABEL ----------------------------------------------------------------
      {
        test: /\.?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },

  //
  // resolve
  //
  resolve: {
    alias: {
      '@assets': path.resolve(__dirname, 'src', 'assets'),
      '@components': path.resolve(__dirname, 'src', 'components'),
      '@game': path.resolve(__dirname, 'src', 'game'),
      '@generated': path.resolve(__dirname, 'src', 'generated'),
      '@modules': path.resolve(__dirname, 'src', 'modules'),
      '@rooms': path.resolve(__dirname, 'src', 'rooms'),
    }
  },

  //
  // plugins
  //
  plugins: [

    // HTML WEBPACK PLUGIN ----------------------------------------------------
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'src', 'index.html')
    }),

    // Copy File --------------------------------------------------------------
    new CopyPlugin({
      patterns: [
        { from: 'src/assets', to: 'assets' }
      ],
    }),
  ]
}
