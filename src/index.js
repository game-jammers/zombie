//
// (c) GameJammers 2021
// http://www.jamming.games
//

import './index.css'
import App from './App.svelte'

const app = new App({
  target: document.body,
  props: {
  }
});
