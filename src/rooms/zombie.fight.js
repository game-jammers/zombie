//
// (c) GameJammers 2021
// http://www.jamming.games
//

import hotkeys from 'hotkeys-js'
import Manifest from '@generated/manifest'

const FLEE_LOCATIONS = [
  'StreetDocks',
  'StreetDowntown',
  'StreetMall',
  'StreetMarket',
  'StreetPark',
  'StreetStripMall',
  'StreetSuburb'
];

//
// ////////////////////////////////////////////////////////////////////////////
//

const FIGHT_TYPE = {
  SINGLE: 0,
  PACK: 1,
  HORDE: 2,
  SEWER: 3
};

//
// intro lines ////////////////////////////////////////////////////////////////
//

const INTRO_LINES = [
  // single zombie
  [
    ['A zombie attacks!'],
    ['A zombie bursts forward!'],
    ['A zombie stumbles at you!'],
  ],

  // zombie pack
  [
    ['You encounter a pack of zombies!'],
  ],

  // zombie horde
  [
    ['You encounter a large horde of zombies!', 'They look terrifying'],
    ['A large horde of zombies blocks your path!', 'There are so many of them!']
  ],

  // sewer horror
  [
    ['A monstrosity the like you have never seen','slithers towards you emenating a smell so powerful','you can barely breathe']
  ]
]

const getIntroLines = (fightType)=>{
  const lines = INTRO_LINES[fightType]
  let result = lines[Math.floor(lines.length*Math.random())]
  return result;
}

//
// zombie names ///////////////////////////////////////////////////////////////
//

const ZOMBIE_TYPES = [
  { name: 'Shambler', hp: [2,3], dmg: [0,1] },
  { name: 'Rotten Face', hp: [2,3], dmg: [0,2] },
  { name: 'Fat Zombie', hp: [2,5], dmg: [1,2] },
  { name: 'One Leg', hp: [1,2], dmg: [1,1] },
  { name: 'Jawless Zombie', hp: [1,2], dmg: [0,3] },
  { name: 'Putrid Zombie', hp: [1,2], dmg: [1,3] },
]

const randomZombie0 = (def)=>{
  return {
      name: def.name,
      hp: Math.round(Math.random()*def.hp[1])+def.hp[0],
      dmg: ()=>Math.floor(Math.random()*def.dmg[1])+def.dmg[0]
  }
}

const randomZombie = ()=>{
  const def = ZOMBIE_TYPES[Math.floor(ZOMBIE_TYPES.length*Math.random())]
  return randomZombie0(def);
}

const SEWER_HORROR = randomZombie0({ name: 'Sewer Horror', hp: [10,5], dmg: [1,2]});

//
// states /////////////////////////////////////////////////////////////////////
//

let STATES = {};

//
// ----------------------------------------------------------------------------
//

STATES.Intro = {
  render: (fight) =>{
    let lines = getIntroLines(fight.fightType);
    let line = 'You encounter ';
    let zambienames = []
    for(var key in fight.zombies) {
      var zombie = fight.zombies[key]
      zambienames.push(zombie.name)
    }
    line = line + zambienames.join(', ')
    line = line.match(/.{1,80}/g)
    lines = [...lines, ...line]
    lines.push('[space] continue')
    return lines;
  },

  mount: (fight)=> {
    hotkeys('space', ()=>{
      fight._changeState(STATES.PlayerTurn);
      return true;
    })
  },

  unmount: (fight)=>{
    hotkeys.unbind('space')
  }
};

//
// ----------------------------------------------------------------------------
//

STATES.PlayerTurn = {
  render: (fight)=>{
    let lines = [];
    if(STATES.PlayerTurn.result) {
      lines = [...STATES.PlayerTurn.result, '', '[space] continue'];
    }
    else {
      lines = [ 
          'PLAYER TURN',
          `HP: ${fight.game.player.hp}`,
          '',
          STATES.PlayerTurn.weapons.join(' | '),
          '[e]scape'
      ];
    }

    return lines;
  },

  mount: (fight)=> {
    STATES.PlayerTurn.result = null;
    STATES.PlayerTurn.bound = []
    STATES.PlayerTurn.runAway = false;
    let weapons = [];
    for(var key in fight.game.weapons) {
      const weapon = fight.game.weapons[key];
      if(weapon.owned) {
        const cmd = weapon.name[0]
        weapons.push(`[${cmd}]${weapon.name.substring(1)}`);
        STATES.PlayerTurn.bound.push(cmd)
        hotkeys(cmd, ()=>{
          if(STATES.PlayerTurn.result) return;

          let msg = [];
          for(var atk = 0; atk < weapon.atks; atk++) {
            if(fight.zombies.length <= 0) break;
            let dmg = weapon.damage[0] + Math.floor(Math.random()*weapon.damage[1]);
            const crit = Math.random() <= weapon.crit;

            const noise = Math.random() <= weapon.noise;
            let newzbs = 0;
            if(noise) newzbs = Math.floor(Math.random()*1+1)

            if(crit) dmg = Math.max(1, dmg * 2);
            let killed = false;
            let zname = fight.zombies[0].name;
            fight.zombies[0].hp -= dmg;
            if(fight.zombies[0].hp <= 0) {
              killed = true;
              fight.game.player.zkills += 1;
              if(fight.zombies.length > 1) {
                fight.zombies = fight.zombies.slice(1);
              }
              else {
                fight.zombies = []
              }
            }

            msg.push(`You ${weapon.verb} with your ${weapon.name}!`)
            if(crit) msg.push(`CRITICAL HIT!`);
            msg.push(`${zname} takes ${dmg} damage`)
            if(killed) msg.push(`${zname}${weapon.deathVerb}`);
            if(noise && newzbs > 0) {
              msg.push(`The noise attracted ${newzbs} zombies!`)
              fight._addRandomZombies(newzbs);
            }
            msg.push('');
          }

          STATES.PlayerTurn.result = msg;
          fight.game.refresh();
        });
      }
    }
    STATES.PlayerTurn.weapons = weapons;

    hotkeys('space', ()=>{
      if(STATES.PlayerTurn.result) {
        STATES.PlayerTurn.result = null;
        if(STATES.PlayerTurn.runAway) {
          fight.zombies = []
          fight.game.changeRoom(FLEE_LOCATIONS[Math.floor(FLEE_LOCATIONS.length*Math.random())])
          fight.game.refresh()
        }
        else {
          fight._changeState(STATES.ZombieTurn);
        }
        return true;
      }
    })

    hotkeys('e', ()=>{
      if(STATES.PlayerTurn.result) return;

      if(fight.game.room.noescape) {
        STATES.PlayerTurn.result = ['There is nowhere to run!!'];
      }
      else {
        let chance = 1.0 - fight.game.time.getUTCHours() / 24.0 + 0.5
        if(Math.random() > chance) {
          STATES.PlayerTurn.result = ['Attempt to escape failed!'];
        }
        else {
          STATES.PlayerTurn.result = ['You run away but got lost.'];
          STATES.PlayerTurn.runAway = true;
        }
      }

      fight.game.refresh();
      return true;
    })
  },

  unmount: (fight)=>{
    hotkeys.unbind('space')
    hotkeys.unbind('e')
    for(var key in STATES.PlayerTurn.bound) {
      var cmd = STATES.PlayerTurn.bound[key]
      if(cmd) {
        hotkeys.unbind(cmd)
      }
    }
  }
};

//
// ----------------------------------------------------------------------------
//

STATES.ZombieTurn = {
  render: (fight)=>{
    if(fight.zombies.length <= 0) {
      return ['The zombies have dispersed'];
    }
    else {
      let msg = ['ZOMBIE TURN']
      let totalDmg = 0
      for(var idx in fight.zombies) {
        let zombie = fight.zombies[idx]
        let dmg = zombie.dmg()
        msg.push(`${zombie.name} attacked dealing ${dmg} damage!`)
        totalDmg += dmg;
      }

      fight.game.player.hp -= totalDmg;
      if(fight.game.player.hp <= 0) { 
          msg.push('YOU ARE DEAD!');
      }
      return msg;
    }
  },

  mount: (fight)=> {
    
    hotkeys('space', ()=>{
      if(fight.game.player.hp > 0) {
        fight._changeState(STATES.PlayerTurn)
      }
      else {
        fight.game.reset();
      }
      return true;
    })
  },

  unmount: (fight)=>{
    hotkeys.unbind('space')
  }
}

//
// ############################################################################
//

export default class ZombieFight {

  //
  // create ///////////////////////////////////////////////////////////////////
  //

  constructor(game) {
    this.game = game;

    if(this.game.player.sewerBossFight) {
      this.fightType = FIGHT_TYPE.SEWER;
      this.zombies = [ SEWER_HORROR ];
      this._scene = Manifest.sewercreature;
      this.wasSewerBoss = true;
    }
    else {
      let rnd = Math.random()
      const h = this.game.time.getUTCHours();
      if(h < 12) {
        rnd = Math.max(0, rnd-0.25)
      }
      else if(h > 19) {
        rnd = Math.min(1, rnd+0.25)
      }

      if(rnd < 0.5) {
        this.fightType = FIGHT_TYPE.SINGLE;
        this.zombies = this._randomZombie(1);
        this._scene = Manifest.zombie;
      }
      else if(rnd < 0.8) {
        this.fightType = FIGHT_TYPE.PACK;
        this.zombies = this._randomZombie(Math.floor(Math.random()*3+2));
        this._scene = Manifest.zombiepack;
      }
      else {
        this.fightType = FIGHT_TYPE.HORDE;
        this.zombies = this._randomZombie(Math.floor(Math.random()*5+5));
        this._scene = Manifest.zombiehorde;
      }
    }

    this.state = STATES.Intro;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get scene() {
    return this._scene;
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  render(r) {
    let result = r._resizeAscii(this.scene.ascii);

    if(this.state) {
      let lines = this.state.render(this, r, result)
      result = r._buildBlock(this, result, lines);
    }

    result = this._renderFightUI(r,result);

    this.finished = this.zombies.length <= 0;

    return result.join('\n')
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    this.state.mount(this);
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    this.state.unmount(this);
  }

  //
  // private methods //////////////////////////////////////////////////////////
  //

  _changeState(newState) {
    this.state.unmount(this);
    this.state = newState;
    this.state.mount(this);
    this.game.refresh();
  }

  //
  // --------------------------------------------------------------------------
  //

  _renderFightUI(r,lines) {
    let topbar = ['Zombie']
    if(this.zombies.length >= 5) {
      topbar[0] = 'Zombie Horde';
    }
    else if(this.zombies.length >= 2) {
      topbar[0] = 'Zombie Pack';
    }

    topbar[1] = `Remaining Zombies: ${this.zombies.length}`
    lines = r._buildBlock(this, lines, topbar, '=', 90, 2);
    
    return lines;
  }

  //
  // --------------------------------------------------------------------------
  //

  _randomZombie(count) {
    let result = []
    for(var i = 0; i < count; i++) {
      result.push(randomZombie());
    }
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  _addRandomZombies(count) {
    for(var i = 0; i < count; i++) {
      this.zombies.push(randomZombie());
    }
  }
  
  
  
}
