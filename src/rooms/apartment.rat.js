//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'
import LOOT from '@rooms/loot'

export default class ApartmentRat {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
    this.eat = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    if(!this.game.inventory.rat) {
      return 'A Dead Rat'
    }
    return 'Empty Cupboards'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    if(!this.game.inventory.rat) {
      return Manifest.rat;
    }
    return Manifest.cupboard;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    if(this.eat) {
      return [
        'The stench as it approaches your lips causes you to gag.',
        'You can\'t bring yourself to do it.',
        'But you also can\'t bring yourself to throw it away.',
        'You place it in your bag.',
        '[B]edroom | B[a]throom | [H]allway'
      ];
    }
    else if(!this.game.inventory.rat) {
      return [
         'You find a dead rat.',
         'Your stomach turns.',
         'You\'re not sure if it\'s from hunger or revulsion.',
         '[e]at it?',
         '[B]edroom | B[a]throom | [H]allway'
      ];
    }
    else {
      return [
        'The cupboards are bare and dusty.',
        'You haven\'t scavenged for supplies in weeks',
        'afraid to leave the house.  But if you want to survive',
        'you\'ll have to brave the outside soon',
        '[B]edroom | B[a]throom | [H]allway'
      ];
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('e', ()=>{
      if(!this.game.inventory.rat) {
        this.eat = true;
        this.game.inventory.rat = {...LOOT.RAT, count: 1};
        this.game.tick(Math.random()*2+1);
      }
    });

    hotkeys('b', ()=> {
      this.game.changeRoom('ApartmentBedroom');
      return true;
    });
    
    hotkeys('a', ()=> {
      this.game.changeRoom('ApartmentBathroom');
      return true;
    });

    hotkeys('h', ()=>{
      this.game.changeRoom('ApartmentHallway');
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
