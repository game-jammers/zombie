//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class ApartmentBedroom {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
    this.rested = false;
    this.ate = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Bedroom'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.bedroom;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    console.log(this.rested, this.ate);
    if(this.rested && this.ate) {
      return [
        `You ate a dinner of ${this.ate} and slept soundly`,
        '',
        '[K]itchen | [B]athroom'
      ]
    }
    else if(this.rested) {
      return [
        'You tried to get some sleep but you don\'t feel terribly rested.',
        '',
        '[K]itchen | [B]athroom'
      ]
    }
    else {
      return [
        'You have been sheltering in your small apartment for months.',
        'Electricity is off, Food is running out, the toilet stopped flusing...',
        'You can safely [r]est here until morning.', 
        '',
        '[K]itchen | [B]athroom',
      ];
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('k', ()=>{
      this.game.changeRoom('ApartmentKitchen');
      return true;
    });
    hotkeys('b', ()=>{
      this.game.changeRoom('ApartmentBathroom');
      return true;
    });
    hotkeys('r', ()=>{
      this.game.time = new Date(this.game.time.getTime() + 1000 * 60 * 60 * 24);
      this.game.time.setUTCHours(6);
      this.game.time.setMinutes(0);
      this.game.tick(Math.random()*4+1);
      this.rested = true;
      this.ate = this.game.removeFood('rat');
      if(this.ate) {
        this.game.player.hp = this.game.maxPlayerHp;
      }
      this.game.refresh();
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
