//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class OverlayInventory {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
    this.selected = null;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return `Inventory - HP ${this.game.player.hp}`
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return {
      ascii: this._getInventoryDisplay(),
      data: []
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
        ...this._getDescription(),
        '[R]eturn to Game',
    ];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('r', ()=>{
      this.game.closeOverlay();
      return true;
    })

    for(var key in this.game.inventory) {
      const item = this.game.inventory[key];
      if(item && item.hotkey) {
        hotkeys(item.hotkey, ()=>{
          this.selected = item;
          this.game.refresh();
          return true;
        })
      }
    }

    for(var id in this.game.weapons) {
      const weapon = this.game.weapons[id];
      if(weapon.owned) {
        const key = weapon.name[0];
        hotkeys(key, ()=>{
          this.selected = weapon;
          this.game.refresh();
        });
      }
    }
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

  //
  // private methods //////////////////////////////////////////////////////////
  //

  _getInventoryDisplay() {
    let result = [
      '',
      '',
      '',
      ''
    ];

    for(var key in this.game.inventory) {
      let entry = this.game.inventory[key];
      let cursor = '-';
      if(this.selected && this.selected.id == entry.id) cursor = '>'
      result.push(`${cursor} [${entry.hotkey}] ${entry.name}: ${entry.count}`)
    }

    var idx = 4;
    for(var key in this.game.weapons) {
      let weapon = this.game.weapons[key]
      if(weapon.owned) {
        let line = result[idx] || ''
        let gutter = 50 - line.length;
        let cursor = '-';
        if(this.selected && this.selected.name == weapon.name) cursor = '>'
        line += `${'&nbsp;'.repeat(gutter)}${cursor} [${weapon.name[0]}]  ${weapon.name} (dmg: ${weapon.damage[0]}-${weapon.damage[0]+weapon.damage[1]} x${weapon.atks})<br />`
        result[idx] = line
        idx += 1;
      }
    }

    return result.join('\n');
  }
  
  //
  // --------------------------------------------------------------------------
  //

  _getDescription() {
    if(this.selected) {
      return this.selected.desc
    }
    return '';
  }
  

}
