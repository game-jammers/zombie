//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class ApartmentBathroom {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
    this.search = false;
    this.rat = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Bathroom' 
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.bathroom;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
        'It stopped flushing two months ago.',
        'You stopped using it last week.',
        'It slowly drains, leaving a dark sludge coating the bottom of the bowl.',
        '',
        '[B]edroom | [K]itchen'
    ];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('b', ()=>{
      this.game.changeRoom('ApartmentBedroom');
      return true;
    })
    hotkeys('k', ()=>{
      this.game.changeRoom('ApartmentKitchen');
      return true;
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
