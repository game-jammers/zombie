//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

const WIN_CONDITION_FOOD_COUNT = 100

export default class ApartmentHall {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Hallway'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.hallway;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
      'The entrance to your apartment.',
      'You can leave, but it gets dangerous at night.',
      'You need to make it back here before dark.',
      '',
      '[K]itchen | [B]edroom | [O]utside'
    ];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {

    // rescue sophie
    if(this.game.player.hasSophie() && !this.game.sophieMode) {
      this.game.changeRoom('EndingSophie');
      this.game.refresh();
      return
    }

    // collect 100 food points
    var food = 0;
    for(var key in this.game.inventory) {
      var item = this.game.inventory[key];
      if(item.food) food += item.food * item.count;
    }
    console.log(`Food ${food} : ${WIN_CONDITION_FOOD_COUNT}`)
    if(food >= WIN_CONDITION_FOOD_COUNT) {
      this.game.changeRoom('EndingFatAndHappy')
      this.game.refresh();
      return;
    }

    hotkeys('o', ()=>{
      this.game.changeRoom('StreetApartment');
      return true;
    });
    hotkeys('k', ()=>{
      this.game.changeRoom('ApartmentKitchen');
      return true;
    });
    hotkeys('b', ()=>{
      this.game.changeRoom('ApartmentBedroom');
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
