//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SewerTunnel extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game)
    this.godown = false;
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds

    ];

    this.noescape = true;
    this.dangerMult = 100;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Sewer Depths'
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get scene() {
    return Manifest.sewer3;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
        'The sewer extends into the darkness.',
    ];

    if(!this.game.player.sewer) {
      result.push('The stench is unbearable');
      result.push('You can sense true evil just ahead.');
    }

    result.push('');
    result.push('[B]ack towards the surface | [C]ontinue deeper...');
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('b', ()=>{
      this.game.changeRoom('SewerPath');
      return true;
    })

    hotkeys('c', ()=>{
      this.game.player.sewerBossFight = true;
      this.game.openOverlay('ZombieFight');
      return true;
    })

    StreetBase.prototype.mount.call(this);
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
