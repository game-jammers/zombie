//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class EndingSophie {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get ending() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get title() {
    return '<3 <3 SOPHIE <3 <3'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.sophie;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
      'You can\'t believe how much Sophie has changed your life',
      'You spend the rest of your lives exploring and adventuring together',
      'It turns out the world is pretty awesome with just the two of you anyway',
      '',
      '',
      '[R]estart'
    ]
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('r', ()=>{
      this.game.reset();
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
