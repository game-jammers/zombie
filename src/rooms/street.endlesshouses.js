//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

const SUBURB_DISTANCE = 30;
const TRAVEL_DISTANCE = 4;
const TRAVEL_TIME = 30;

const SUBURB_SCENES = [
  Manifest.street1,
  Manifest.street2,
  Manifest.street3,
  Manifest.street4,
  Manifest.street5,
  Manifest.street6,
  Manifest.street7,
  Manifest.street8,
  Manifest.street9
]

export default class StreetEndlessHouses extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game,extra) {
    super(game)
    extra = extra || { reverse: false };
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: 0.10, data: LOOT.PIGEON },
        { chance: 0.05, data: LOOT.COKE },
        { chance: 0.08, data: LOOT.SOUP },
        { chance: 0.11, data: LOOT.CHIPS },
        { chance: 0.15, data: LOOT.DOGFOOD },
        { chance: 0.15, data: LOOT.BAT },
        { chance: 0.10, data: LOOT.KNIFE },
        { chance: 0.05, data: LOOT.AXE }
      ];

    this.distance = TRAVEL_DISTANCE;
    if(extra.reverse) this.distance = SUBURB_DISTANCE-TRAVEL_DISTANCE;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get dangerMult() {
    return this.distance * 10;
  }

  get title() {
    return `Suburbs (${Math.round(this.distance)}km from the City`
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get scene() {
    return SUBURB_SCENES[Math.floor(SUBURB_SCENES.length*Math.random())];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ `Endless houses, one after another.`,
               `This road leads to a National Forest ${Math.round(SUBURB_DISTANCE-this.distance)}km away` ,
               `Zombies will get more numerous along the way.`
    ]

    result.push('');
    result.push('[H]ead back towards City | [C]ontinue forward...');
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('h', ()=>{
      this.distance -= Math.random()*TRAVEL_DISTANCE+TRAVEL_DISTANCE
      if(this.distance <= 0) {
        this.game.changeRoom('StreetSuburb');
      }
      else {
        this.game.tick(TRAVEL_TIME*Math.random()+TRAVEL_TIME);
      }
      return true;
    })

    hotkeys('c', ()=>{
      this.distance += Math.random()*TRAVEL_DISTANCE+TRAVEL_DISTANCE
      if(this.distance >= SUBURB_DISTANCE) {
        this.game.changeRoom('Forest',TRAVEL_TIME*Math.random()+TRAVEL_TIME);
      }
      else {
        this.game.tick(TRAVEL_TIME*Math.random()+TRAVEL_TIME);
      }
      return true;
    })

    StreetBase.prototype.mount.call(this);
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
