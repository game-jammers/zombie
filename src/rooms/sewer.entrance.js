//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SewerEntrance extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game)
    this.godown = false;
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds

    ];

    this.noescape = true;
    this.dangerMult = 10;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Sewer Entrance'
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get scene() {
    return Manifest.sewer1;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
        'The sewer extends into the darkness.',
    ];

    if(!this.game.player.sewer) {
      result.push('The stench is strongly emenating from deep within');
    }

    result.push('');
    result.push('[E]xit to Apartment Street | [C]ontinue deeper...');
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('a', ()=>{
      this.game.changeRoom('ApartmentHallway');
      return true;
    })

    hotkeys('c', ()=>{
      this.game.changeRoom('SewerPath');
      return true;
    })

    StreetBase.prototype.mount.call(this);
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
