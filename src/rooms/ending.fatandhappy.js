//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class EndingFatAndHappy {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get ending() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get title() {
    return 'Fat And Happy'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.fatandhappy;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
      'You have learned how to survive',
      'and collected enough food to last you at least another year',
      'Maybe it\'s not much.',
      'But it will be enough.',
      '[R]estart'
    ]
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('r', ()=>{
      this.game.reset();
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
