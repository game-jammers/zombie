//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetDocks extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game)
    this.search = false;
    this.rat = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Wharf'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street7;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'The shipping wharf was once bustling with commercial freight.',
      'Even this close to the ocean the air is heavy and still.',
      this.game.timeString,
      '',
      '[A]partment | [D]owntown | [S]tripmall'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('a', ()=>{
      this.game.changeRoom('StreetApartment', Math.random()*10+20);
      return true;
    })
    hotkeys('d', ()=>{
      this.game.changeRoom('StreetDowntown', Math.random()*10+20);
      return true;
    })
    hotkeys('s', ()=>{
      this.game.changeRoom('StreetStripMall', Math.random()*10+20);
      return true;
    })



  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
