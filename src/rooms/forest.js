//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

const FOREST_DISTANCE = 50;
const TRAVEL_DISTANCE = 4;
const TRAVEL_TIME = 60;

const FOREST_SCENES = [
  Manifest.forest,
  Manifest.forest2,
  Manifest.forest3,
  Manifest.forest4
]

export default class Forest extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game)
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null }, // dead drop to influence odds
    ];

    this.distance = TRAVEL_DISTANCE;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get dangerMult() {
    return (FOREST_DISTANCE-this.distance)*10;
  }

  //
  // --------------------------------------------------------------------------
  //

  get title() {
    return `Forest (${Math.round(this.distance)}km from the City`
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get scene() {
    return FOREST_SCENES[Math.floor(FOREST_SCENES.length*Math.random())];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 'A deep and lucious forest, you are not sure how far it goes' ]

    result.push('');
    result.push('[H]ead back towards City | [C]ontinue forward...');
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('h', ()=>{
      this.distance -= Math.random()*TRAVEL_DISTANCE+TRAVEL_DISTANCE
      if(this.distance <= 0) {
        this.game.changeRoom('StreetEndlessHouses',TRAVEL_TIME,{reverse: true});
      }
      else {
        this.game.tick(TRAVEL_TIME);
      }
      return true;
    })

    hotkeys('c', ()=>{
      this.distance += Math.random()*TRAVEL_DISTANCE+TRAVEL_DISTANCE
      if(this.distance >= FOREST_DISTANCE) {
        this.game.changeRoom('EndingForest');
      }
      else {
        this.game.tick(TRAVEL_TIME);
      }
      return true;
    })

    StreetBase.prototype.mount.call(this);
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
