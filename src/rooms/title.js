//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class TitleRoom {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get showTime() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get title() {
    if(this.game.sophieMode) {
      return 'CTRL+ALT SOPHIE ESC'
    }
    else {
      return 'CTRL+ALT ZOMBIE ESC'
    }
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    if(this.game.sophieMode) {
      return Manifest.sophie;
    }
    else {
      return Manifest.zombie;
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    if(this.game.sophieMode) {
      return [
        'A 52 hour solo game jam',
        'About living with your best friend Sophie',
        'The best doggo and shelter in place partner ever.',
        'https://jamming.games/',
        '[S]tart | [?] Help'
      ];
    }
    
    return [
      'A 52 hour solo game jam',
      'About the zombie apocalypse in the style of a DOS adventure game',
      '',
      'https://jamming.games/',
      '[S]tart | [?] Help',
      '',
      `${this.game.version}`
    ];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('s', ()=>{
      this.game.changeRoom('ApartmentBedroom');
      return true;
    });
    hotkeys('ctrl+shift+alt+s', ()=>{
      this.game.sophieMode = true;
      this.game.refresh();
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind('s');
  }

}
