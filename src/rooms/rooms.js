//
// (c) GameJammers 2021
// http://www.jamming.games
//

import ApartmentBathroom from '@rooms/apartment.bathroom';
import ApartmentBedroom from '@rooms/apartment.bedroom';
import ApartmentHallway from '@rooms/apartment.hallway';
import ApartmentKitchen from '@rooms/apartment.kitchen';
import ApartmentRat from '@rooms/apartment.rat';

import EndingFatAndHappy from '@rooms/ending.fatandhappy';
import EndingForest from '@rooms/ending.forest';
import EndingSewer from '@rooms/ending.sewer';
import EndingSophie from '@rooms/ending.sophie';

import Forest from '@rooms/forest';

import OverlayHelp from '@rooms/overlay.help';
import OverlayInventory from '@rooms/overlay.inventory';

import SewerEntrance from '@rooms/sewer.entrance';
import SewerPath from '@rooms/sewer.path';
import SewerTunnel from '@rooms/sewer.tunnel';

import StreetApartment from '@rooms/street.apartment';
import StreetDocks from '@rooms/street.docks';
import StreetDowntown from '@rooms/street.downtown';
import StreetEndlessHouses from '@rooms/street.endlesshouses';
import StreetMall from '@rooms/street.mall';
import StreetMarket from '@rooms/street.market';
import StreetOffice from '@rooms/street.office';
import StreetPark from '@rooms/street.park';
import StreetPolice from '@rooms/street.police';
import StreetStripMall from '@rooms/street.stripmall';
import StreetSuburb from '@rooms/street.suburb';

import TitleRoom from '@rooms/title';

import ZombieFight from '@rooms/zombie.fight';

const rooms = {
  ApartmentBathroom,
  ApartmentBedroom,
  ApartmentHallway,
  ApartmentKitchen,
  ApartmentRat,

  EndingFatAndHappy,
  EndingForest,
  EndingSewer,
  EndingSophie,

  Forest,

  OverlayHelp,
  OverlayInventory,

  SewerEntrance,
  SewerPath,
  SewerTunnel,

  StreetApartment,
  StreetDocks,
  StreetDowntown,
  StreetEndlessHouses,
  StreetMall,
  StreetMarket,
  StreetOffice,
  StreetPark,
  StreetPolice,
  StreetStripMall,
  StreetSuburb,

  TitleRoom,

  ZombieFight
}

export default rooms;
