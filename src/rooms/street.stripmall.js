//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetStripMall extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.search = false;
    this.rat = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Strip Mall'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street9;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'Niche stores and shopes that rely heavily on foot traffic',
      this.game.timeString,
      '',
      '',
      '[D]ocks | [M]arket'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('d', ()=>{
      this.game.changeRoom('StreetDocks', Math.random()*10+45);
      return true;
    })

    hotkeys('m', ()=>{
      this.game.changeRoom('StreetMarket', Math.random()*10+30);
      return true;
    })

  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
