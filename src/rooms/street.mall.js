//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetMall extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: 0.05, data: LOOT.PIGEON },
        { chance: 0.08, data: LOOT.SOUP },
        { chance: 0.11, data: LOOT.CHIPS },
        { chance: 0.10, data: LOOT.KNIFE },
        { chance: 0.05, data: LOOT.CROSSBOW }
      ];

  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Mall'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street2;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'A massive structure filled with ever kind of shop and store you can imagine',
      this.game.timeString,
      '',
      '',
      '[S]uburbs'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('s', ()=>{
      this.game.changeRoom('StreetSuburb', Math.random()*10+35);
      return true;
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
