//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'
import LOOT from '@rooms/loot' 

export default class SteetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //

  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return true;
  }

  //
  // virtual methods //////////////////////////////////////////////////////////
  //
  
  mount() {
    hotkeys('shift+s', ()=>{
      let ltable = this.loot_table || [ 
        { chance: 0.01, data: LOOT.CHIPS },
        { chance: 0.01, data: LOOT.PIGEON },
        { chance: 0.01, data: LOOT.COKE },
      ];

      let loot = ltable[Math.floor(ltable.length*Math.random())]
      let found = false;
      if(Math.random() < loot.chance) {
        if(loot.data.weapon) {
          if(!this.game.weapons[loot.data.id].owned) {
            found = true;
            this.game.weapons[loot.data.id].owned = true;
          }
        }
        else {
          found = true;
          let data = this.game.inventory[loot.data.id] || loot.data;
          data.count = data.count+1 || 1;
          this.game.inventory[loot.data.id] = data;
        }
      }

      if(found) {
        this.msg = `You found ${loot.data.name}!`;
      }
      else {
        this.msg = 'Your search unfortunately proved fruitless';
      }
      this.game.tick(10);
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  postBody(body) {
    if(this.msg) {
      body.splice(0,0,this.msg);
      this.msg = null;
    }

    if(this.game.player.hasSophie()) {
      if(Math.random() < 0.4) {
        body.splice(-1,0,'A zombie shambled at you, but Sophie woof woofed it away');
      }
      body.splice(-1,0,'Sophie dutifully follows you, her tail wagging.');
    }
    
    body.push('[Shift+S]earch Area for 30 minutes')
    return body;
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
  }
}
