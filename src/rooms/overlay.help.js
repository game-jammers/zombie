//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class OverlayHelp {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Help'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    let spoiler = '';
    if(this.spoilers) {
      spoiler = `
SPOILER<br />
The easiest way to win is to collect 100 food points and return to your aparment<br />
SPAM is worth 10 food points!<br />
Want more... like a [c]heat code or something?<br />
`
    }

    let cheat = '';
    if(this.cheat) {
      cheat = `
CHEAT<br />
Press CTRL+SHIFT+ALT+S at the Title screen to engage Sophie Mode<br />
Some endings won't be available this way.<br />
`
    }

    return {
      data: [],
      ascii: `
<br />
<br />
<br />
<br />
Press characters surrounded by square brackets to perform that action. ie: [s]<br />
- [?] Opens up this Help - but you figured that out already <br />
- [Shift+I] Opens up your inventory.<br />
- When outside you can [Shift+S] to spend ~30 minutes searching the area.<br />
- Even if you don't find anything in 30 minutes doesn't mean there is nothing to find!<br />
- There are 4 endings (with some variation) you can get based on how you play.<br />
- Resting in your bedroom will let you skip night and start at 6am the next day<br />
- Resting also will heal you if you have food to eat.<br />
- You can increase your max HP by killing zombies and resting<br />
- Do you want some extra help with [s]poilers?<br />
<br />
${spoiler}
${cheat}
`
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
        '[R]eturn to Game',
    ];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys.unbind();
    hotkeys('r', ()=>{
      this.game.closeOverlay();
      return true;
    })
    hotkeys('s', ()=>{
      this.spoilers = true;
      this.game.refresh();
    })
    hotkeys('c', ()=>{
      if(this.spoilers) {
        this.cheat = true;
        this.game.refresh();
      }
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
