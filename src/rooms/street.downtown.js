//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetDowntown extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: -1, data: null },
        { chance: 0.15, data: LOOT.COKE },
        { chance: 0.21, data: LOOT.CHIPS },
        { chance: 0.15, data: LOOT.PIGEON },
        { chance: 0.15, data: LOOT.KNIFE },
      ];

  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Downtown'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street5;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'Rows of shops and restaurants line the sidewalk',
      'Above them sit apartments once too expensive for you to have afforded them',
      this.game.timeString,
      '',
      '[D]ocks | [P]olice Station'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('d', ()=>{
      this.game.changeRoom('StreetDocks', Math.random()*10+34);
      return true;
    })
    hotkeys('p', ()=>{
      this.game.changeRoom('StreetPolice', Math.random()*10+38);
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
