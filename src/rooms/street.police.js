//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class StreetPolice extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: -1, data: null },
        { chance: -1, data: null },
        { chance: 0.15, data: LOOT.COKE },
        { chance: 0.21, data: LOOT.CHIPS },
        { chance: 0.15, data: LOOT.COKE },
        { chance: 0.21, data: LOOT.CHIPS },
        { chance: 0.10, data: LOOT.PISTOL },
        { chance: 0.05, data: LOOT.SHOTGUN },
      ];

  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Police Station'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.police;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'The police building was renovated only months before the outbreak',
      this.game.timeString,
      '',
      '[D]owntown'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('d', ()=>{
      this.game.changeRoom('StreetDowntown', Math.random()*10+34);
      return true;
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
