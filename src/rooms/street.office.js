//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class StreetOffice extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: 0.15, data: LOOT.COKE },
        { chance: 0.30, data: LOOT.CHIPS },
        { chance: 0.20, data: LOOT.CROWBAR },
      ];

  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Office Complex'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street4;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'A large glass office complex.',
      this.game.timeString,
      '',
      '',
      '[S]uburbs'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('s', ()=>{
      this.game.changeRoom('StreetSuburb', Math.random()*10+35);
      return true;
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
