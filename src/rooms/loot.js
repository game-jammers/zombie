//
// (c) GameJammers 2021
// http://www.jamming.games
//

// jlnqrtvwyz

const LOOT = {
  RAT: { name: 'Dead Rat', id: 'rat', food: 1, idis: '[D]ead Rat',          hotkey: 'd', desc: ['A dead rat.', 'You cannot bring yourself to eat it', 'Nor you can you convince yourself to throw it away'] },
  FISH: { name: 'Rotten Fish', id: 'fish', food: 2, idis: '[R]otten Fish',  hotkey: 'r', desc: ['A slimy, smelly fish', 'It tastes like mud but it\'s better than nothing'] },
  SPAM: { name: 'SPAM', id: 'spam', food: 10, idis: 'SPA[M]',               hotkey: 'm', desc: ['A canned jelly meat product', 'Before you were pretty snobby about never eating these.', 'Now it\'s your favorite treat'] },
  PEAS: { name: 'Canned Peas', food: 2, id: 'peas', idis: 'Canned P[e]as',  hotkey: 'e', desc: ['A can of peas', 'Not very filling but the only green thing you can eat anymore'] },
  SOUP: { name: 'Noodle Soup', food: 3, id: 'soup', idis: 'Canned So[u]p',  hotkey: 'u', desc: ['Your mom used to make this for you when you were sick', 'Even though it is not very filling','It is still your favorite food on rainy days'] },
  PIGEON: { name: 'Pigeon', food: 1, id: 'pigeon', idis: 'P[i]geon',        hotkey: 'i', desc: ['A dead pigeon.', 'Not much to look at but tastes like chicken'] },
  COKE: { name: 'A can of Coke', food: 1, id: 'coke', idis: 'C[o]ke',       hotkey: 'o', desc: ['A can of coke', 'It is sugary and makes your teeth hurt', 'But tastes like summer'] },
  CHIPS: { name: 'Potato Chips', food: 2, id: 'chips', idis: 'C[h]ips',     hotkey: 'h', desc: ['A salty cruncy bag of potato chips', 'Not very filling but they make you happy anyway'] },
  
  DOGFOOD: { name: 'Dog Food', id: 'dogfood', idis: 'Do[g] Food', hotkey: 'g', desc: ['It smells awful but doesn\'t taste too bad.', 'Your dog used to love this stuff.'] },

  BAT: { name: 'Baseball Bat', id: 'bat', weapon: true },
  KNIFE: { name: 'Knife', id: 'knife', weapon: true },
  AXE: { name: 'Axe', id: 'axe', weapon: true },
  CROWBAR: { name: 'Crowbar', id: 'crowbar', weapon: true },
  PISTOL: { name: 'Pistol', id: 'pistol', weapon: true },
  SHOTGUN: { name: 'Shotgun', id: 'shotgun', weapon: true },
  CROSSBOW: { name: 'Crossbow', id: 'crossbow', weapon: true },
}

export default LOOT;
