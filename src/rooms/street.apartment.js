//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetApartment extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game)
    this.godown = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Apartment Street'
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get scene() {
    return Manifest.street1;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = []
    if(!this.game.weapons.crowbar.owned) {
      result = [ 
        'You are outside your [A]partment.',
        this.game.timeString,
        '',
      ];
    }
    else {
      this.godown = true;
      result = [
        'You smell an incredibly intense putrid odor from a sewer grate',
        'You can see a manhole cover that your crowbar could open',
        'Do you dare [g]o down?'
      ];
    }
    result = [...result, '', '[D]ocks | [P]ark']

    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('a', ()=>{
      this.game.changeRoom('ApartmentHallway');
      return true;
    })

    hotkeys('d', ()=>{
      this.game.changeRoom('StreetDocks', Math.random()*10+30);
      return true;
    })

    hotkeys('p', ()=>{
      this.game.changeRoom('StreetPark', Math.random()*10+45);
      return true;
    })

    hotkeys('g', ()=>{
      if(this.game.weapons.crowbar.owned) {
        this.game.changeRoom('SewerEntrance', Math.random()*5+5);
      }
    })

    StreetBase.prototype.mount.call(this);
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
