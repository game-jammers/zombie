//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class EndingForest {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get ending() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get title() {
    return 'Freedom';
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.dawn;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    if(this.game.player.hasSophie()) {
      return [
        'Far from the city the air is clean and the food plentiful',
        'You and Sophie explore the untamed wilderness together',
        'And find happiness and peace amongst the woods and fields',
        '',
        '[R]estart'
      ]
    }
    else {
      return [
        'You journey out past the city, where the air becomes clean',
        'A feeling of peace and calm fills you, and you know you will surive',
        'Now the only danger is your loneliness.',
        '',
        '[R]estart'
      ]
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('r', ()=>{
      this.game.reset();
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
