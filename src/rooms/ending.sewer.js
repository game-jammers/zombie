//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class EndingSewer {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get ending() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get title() {
    return 'A New Dawn';
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.dawn;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    if(this.game.player.hasSophie()) {
      return [
        'You are not sure how that creature was related to the outbreak',
        'But after it died, the zombies slowly seemed to disappear',
        'It has been six months since you last saw an infected.',
        'Now the entire world is a playground for you and Sophie',
        '',
        '[R]estart'
      ]
    }
    else {
      return [
        'You are not sure how that creature was related to the outbreak',
        'But after it died, the zombies slowly seemed to disappear',
        'It has been six months since you last saw an infected.',
        'Now the only danger is your loneliness.',
        '',
        '[R]estart'
      ]
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('r', ()=>{
      this.game.reset();
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
