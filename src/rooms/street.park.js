//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

const SOPHIE_CHANCE = 0.25

export default class SteetPark extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.search = false;

    this.sophieMsg = [];
    this.fedsoph = false;
    if(this.game.player.sophie < 3 && Math.random() < SOPHIE_CHANCE) {
        switch(this.game.player.sophie) {
          case 0: this.sophieMsg = [ '', 'You see a scrawny doggo in the bushes', 'She is very wary of you.' ]; break;
          case 1: this.sophieMsg = [ '', 'You see a dirty doggo in the bushes', 'She eyes you suspiciously' ]; break;
          case 2: this.sophieMsg = [ '', 'You see Sophie in the bushes', 'Her tail is wagging' ]
        }

        if(this.game.hasItem('dogfood')) {
          this.offer = true;
          this.sophieMsg.push('You have some dog food.  [O]ffer her some?');
        }
    }
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Community Park'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street6;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [];
    if(!this.fedSoph) {
      result = [ 
        'A small community park.  The equipment was in disrepair before',
        'now it\'s just so much rust and broken glass.',
        this.game.timeString,
      ];

      result = [...result, 
        ...this.sophieMsg,
      ];
    }
    else {
      switch(this.game.player.sophie) {
        case 1: result = [
          'You place the food on the ground and step away',
          'The dog slinks out slowly sniffing the ground',
          'She eats the food in one big gulp, then runs away'
        ]; break;
        case 2: result = [
          'The pupper watches you closely as she walks to the food',
          'You can see her tail slowly wag as she eats',
          'You get close enough to see a name tag before she runs away',
          'It reads: SOPHIE'
        ]; break;
        case 3: result = [
          'Sophie comes to you as soon as you get the can out of your bag',
          'Her tail is wagging and she is giving you a doggy grin',
          'After she eats she continues to follow you.'
        ]; break;
      }
    }

    result = [...result,
      '',
      '[A]partment | [S]uburb'
    ];

    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('a', ()=>{
      this.game.changeRoom('StreetApartment', Math.random()*10+20);
      return true;
    })

    hotkeys('s', ()=>{
      this.game.changeRoom('StreetSuburb', Math.random()*10+40);
      return true;
    })

    hotkeys('o', ()=>{
      if(this.offer && !this.fedSoph) {
        this.game.removeItem('dogfood');
        this.game.player.sophie += 1;
        this.fedSoph = true;
        this.game.refresh();
        return true;
      }
    })

  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
