//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetSuburb extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.search = false;
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: 0.10, data: LOOT.PIGEON },
        { chance: 0.05, data: LOOT.COKE },
        { chance: 0.08, data: LOOT.SOUP },
        { chance: 0.11, data: LOOT.CHIPS },
        { chance: 0.15, data: LOOT.DOGFOOD },
        { chance: 0.15, data: LOOT.BAT },
        { chance: 0.10, data: LOOT.KNIFE },
        { chance: 0.05, data: LOOT.AXE }
      ];
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Residential Streets'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street3;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'Rows of houses, each built using subtle variations on the same template',
      this.game.timeString,
      '',
      'They seem to go on [f]orever.',
      '',
      '[P]ark | [O]ffice | [M]all'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('p', ()=>{
      this.game.changeRoom('StreetPark', Math.random()*10+20);
      return true;
    })

    hotkeys('o', ()=>{
      this.game.changeRoom('StreetOffice', Math.random()*10+18);
      return true;
    })

    hotkeys('m', ()=>{
      this.game.changeRoom('StreetMall', Math.random()*10+30);
      return true;
    })

    hotkeys('f', ()=>{
      this.game.changeRoom('StreetEndlessHouses', Math.random()*10+30);
      return true;
    })
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
