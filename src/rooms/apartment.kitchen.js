//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

export default class ApartmentKitchen {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    this.game = game;
    this.search = false;
    this.rat = false;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Kitchen'
  }

  //
  // --------------------------------------------------------------------------
  //

  get showTime() {
    return true;
  }

  //
  // --------------------------------------------------------------------------
  //

  get isDangerous() {
    return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.kitchen;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    return [
        'The fridge is as empty as your stomach.',
        'Perhaps there is something in the [c]upboards?',
        '',
        '',
        '[B]edroom | B[a]throom | [H]allway'
    ];
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    hotkeys('c', ()=>{
      this.game.changeRoom('ApartmentRat');
      return true;
    })
    hotkeys('b', ()=>{
      this.game.changeRoom('ApartmentBedroom');
      return true;
    })
    hotkeys('a', ()=>{
      this.game.changeRoom('ApartmentBathroom');
    })
    hotkeys('h', ()=>{
      this.game.changeRoom('ApartmentHallway');
      return true;
    });
  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    hotkeys.unbind();
  }

}
