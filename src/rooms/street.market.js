//
// (c) GameJammers 2021
// http://www.jamming.games
//

import StreetBase from '@rooms/street.base'
import Manifest from '@generated/manifest.js'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'

export default class SteetMarket extends StreetBase {

  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor(game) {
    super(game);
    this.search = false;
    this.rat = false;
    this.loot_table = [ 
        { chance: -1, data: null }, // dead drop to influence odds
        { chance: -1, data: null },
        { chance: 0.15, data: LOOT.SPAM }, 
        { chance: 0.20, data: LOOT.PEAS },
        { chance: 0.15, data: LOOT.COKE },
        { chance: 0.18, data: LOOT.SOUP },
        { chance: 0.21, data: LOOT.CHIPS },
        { chance: 0.10, data: LOOT.DOGFOOD },
      ];

  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get title() {
    return 'Market Street'
  }

  //
  // --------------------------------------------------------------------------
  //

  get scene() {
    return Manifest.street8;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  get body() {
    let result = [ 
      'A large open air market where peddlers would hock their wares',
      this.game.timeString,
      '',
      '[S]trip Mall'
    ];
    result = StreetBase.prototype.postBody.call(this, result);
    return result;
  }

  //
  // --------------------------------------------------------------------------
  //
  
  mount() {
    StreetBase.prototype.mount.call(this);
    hotkeys('s', ()=>{
      this.game.changeRoom('StreetStripMall', Math.random()*10+30);
      return true;
    })

  }

  //
  // --------------------------------------------------------------------------
  //

  unmount() {
    StreetBase.prototype.unmount.call(this);
    hotkeys.unbind();
  }

}
