//
// (c) GameJammers 2021
// http://www.jamming.games
//

import Manifest from '@generated/manifest';
import dateformat from 'dateformat';

const kCOL_COUNT = 100
const kROW_COUNT = 40

export default class Render {
  //
  // create ///////////////////////////////////////////////////////////////////
  //
  constructor() {
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  render(room, time) {

    if(room.render) {
      return room.render(this);
    }

    let result = this._resizeAscii(room.scene.ascii)

    //
    // add title
    //
    result[1] = this._buildLine(room,'',1,90,'*','-');
    result[2] = this._buildLine(room,room.title,2,98,'*','-');
    if(time && room.showTime) {
      let timef = dateformat(time, 'UTC:dddd, mmmm dd, yyyy H:MM:ss TT')
      result[3] = this._buildLine(room,timef,3,90,'*','&nbsp',);
    }
    else {
      result[3] = this._buildLine(room,'',3,90,'*','-');
    }

    //
    // add body
    //
    result = this._buildBlock(room,result,room.body);

    //
    // done
    //
    return result.join('\n')
  }
  
  //
  // private methods //////////////////////////////////////////////////////////
  //

  _resizeAscii(ascii) {
    let result = ascii.split('\n');

    //
    // normalize size of all images
    //
    if(result.length < kROW_COUNT) {
      let diff = kROW_COUNT - result.length
      let end = new Array(diff).fill('<span">&nbsp;</span>'.repeat(kCOL_COUNT))
      result = [ ...result, ...end ]
    }
    else {
      result = result.slice(0,40);
    }

    return result;
  }

  //
  // --------------------------------------------------------------------------
  //

  _buildBlock(room,result,lines,border,size,row) {
    border = border || '='
    const fill = '&nbsp;'
    row = row || kROW_COUNT - 4 - lines.length
    size = size || 90

    result[row] = this._buildLine(room, '', row, size, border, border);
    for(var y = 0; y < lines.length; y++) {
      const wrow = row + y + 1
      result[wrow] = this._buildLine(room, lines[y] || '', wrow, size, border, fill);
    }
    result[row+lines.length+1] = this._buildLine(room, '', row, size, border, border);

    return result;
  }
  

  //
  // --------------------------------------------------------------------------
  //

  _buildLine(room,text,row,size,border,fill) {
    border = border || '*'
    fill = fill || '&nbsp;'

    let edge = (kCOL_COUNT - size)/2;
    let actualSize = size - border.length - border.length-1;

    let result = this._getBlock(room,row,0,edge)
    result += border;

    if(text.length > 0) {
      let fills = (actualSize - text.length)/2
      result += fill.repeat(fills-1);
      result += ` ${text} `;
      result += fill.repeat(fills-1);
    }
    else {
      result += fill.repeat(actualSize);
    }

    result += border;
    result += this._getBlock(room,row,kCOL_COUNT-edge,kCOL_COUNT);

    return result;
  }

  //
  // --------------------------------------------------------------------------
  //

  _getText(room,x,y) {
    const idx = x + y * kCOL_COUNT;
    const data = room.scene.data[idx] || {glyph: 'x', color: {r: 0, g: 0, b: 0, a: 0.0}};
    return `<span style="color:rgba(${data.color.r}, ${data.color.g}, ${data.color.b}, ${data.color.a})">${data.glyph}</span>`;
  }

  //
  // --------------------------------------------------------------------------
  //

  _getBlock(room, row, from, to) {
    let result = []
    for(var x=from; x<to; x++) {
      result.push(this._getText(room,x,row))
    }
    return result.join('')
  }
}
