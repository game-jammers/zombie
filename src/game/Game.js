//
// (c) GameJammers 2021
// http://www.jamming.games
//

import { writable } from 'svelte/store'
import Render from '@game/Render'
import rooms from '@rooms/rooms'
import hotkeys from 'hotkeys-js'

import LOOT from '@rooms/loot'
import PACKAGE from '../../package.json'

export default class Game {
  //
  // create ///////////////////////////////////////////////////////////////////
  //

  constructor() {
    this.display = new writable('');
    this.render = new Render();
    this.reset();
    this.lastFightCheck = 0;
  }

  //
  // public methods ///////////////////////////////////////////////////////////
  //

  get version() {
      return `v${PACKAGE.version}`
  }

  reset() {
    if(this.overlay) this.overlay.unmount();
    if(this.room) this.room.unmount();

    this.ticks = 0;
    this._update = this._update.bind(this);
    this.room = null;
    this.time = new Date('January 12, 1992 06:00:00 UTC');
    this.overlay = null;
    this.player = {
        hp: this.maxPlayerHp,
        sophie: 0,
        hasSophie: ()=>{ return this.player.sophie >= 3 || this.sophieMode },
        zkills: 0,
        sewer: false,
    }

    this.weapons = {
        fists: { owned: true, name: 'Fists',    damage: [0,2], crit: 0.15, noise: 0.00, atks: 1, verb: 'punch',             deathVerb: ' crumples to the ground.',              desc: [ 'Your hands have become bruised and calloused' ] },
        bat: {                name: 'Bat',      damage: [1,4], crit: 0.20, noise: 0.00, atks: 2, verb: 'swing',             deathVerb: '\'s head cracks open like a melon.',    desc: ['An aluminum baseball bat', 'It is lighter and shorter than you remember as a kid.'] },
        knife: {              name: 'Knife',    damage: [0,2], crit: 0.50, noise: 0.00, atks: 3, verb: 'stab',              deathVerb: '\'s head is penetrated by your blade.', desc: ['A simple fixed blade hunting knife', 'It is hard to use, but it is very quiet', 'Also has a high chance of critical hits'] },
        crowbar: {            name: 'Crowbar',  damage: [2,2], crit: 0.25, noise: 0.05, atks: 1, verb: 'go Gordon Freeman', deathVerb: ' splatters into a gory mush.',          desc: ['A heavy iron crowbar']},
        axe: {                name: 'Axe',      damage: [2,3], crit: 0.25, noise: 0.10, atks: 1, verb: 'chop',              deathVerb: '\'s head is split open.',               desc: ['A fire axe with a long wooden handle']},
        crossbow: {           name: 'XBow',     damage: [4,3], crit: 0.25, noise: 0.00, atks: 1, verb: 'snipe',             deathVerb: '\'s head snaps back and collapses',     desc: ['A powerful and silent hunting crossbow']},
        pistol: {             name: 'Pistol',   damage: [3,2], crit: 0.10, noise: 0.25, atks: 1, verb: 'take a shot',       deathVerb: ' goes down with a clean headshot.',     desc: ['A standard issue police firearm']},
        shotgun: {            name: 'Shotgun',  damage: [2,3], crit: 0.50, noise: 0.15, atks: 4, verb: 'let loose',         deathVerb: ' is reduced to a putrid mist.',         desc: ['A loud zombie killing pump action shotgun']},
    }

    this.inventory = {
    };

    this.changeRoom('TitleRoom');
  }

  start() {
    setTimeout(this._update, 15);
  }

  get timeOfDay() {
    const hours = this.time.getUTCHours();
    if(hours < 6) {
        return 'night';
    }
    else if(hours < 11) {
        return 'morning';
    }
    else if(hours < 16) {
        return 'afternoon';
    }
    else if(hours < 18) {
        return 'evening';
    }
    else {
        return 'night';
    }
  }

  //
  // --------------------------------------------------------------------------
  //

  mount(element) {
  }

  //
  // --------------------------------------------------------------------------
  //

  tick(dt) {
    this.ticks += 1;
    if(!dt) {
      console.log('TICK CALLED WITH NO DT')
      dt = 0;
    }
    this.time = new Date(this.time.getTime() + ( dt * 1000 * 60));

    //
    // check for zombie encounter
    //
    const mult = dt / 30;
    if(this.room.isDangerous && !this.player.hasSophie()) {
      let chance = 0.0
      switch(this.timeOfDay) {
        case 'morning': chance = 0.1; break;
        case 'afternoon': chance = 0.25; break;
        case 'evening': chance = 0.33; break;
        case 'night': chance = 0.66; break;
      }
      
      
      chance = chance * mult * (this.room.dangerMult || 1)
      console.log(chance);

      if(Math.random() <= chance) {
        this.openOverlay('ZombieFight');
      }
    }

    this.refresh();
  }

  //
  // --------------------------------------------------------------------------
  //

  refresh() {
    if(this.overlay) {
      if(this.overlay.finished) {
        const sewerBossWin = this.overlay.wasSewerBoss || false
        this.closeOverlay();
        if(sewerBossWin) {
          this.changeRoom('EndingSewer');
        }
        return;
      }
      this.display.set(this.render.render(this.overlay));
    }
    else {
      this.display.set(this.render.render(this.room,this.time));
    }
  }

  //
  // --------------------------------------------------------------------------
  //
  
  changeRoom(room, dt, extra) {
    if(this.room) {
      this.room.unmount();
    }

    hotkeys.unbind();

    this.room = new rooms[room](this, extra);
    this.room.mount(this);
    if(!dt) {
      dt = Math.random() * 4 + 1;
    }

    this._bindHotkeys();
    this.tick(dt);
  }

  //
  // --------------------------------------------------------------------------
  //

  openOverlay(name) {
    hotkeys.unbind();
    if(this.overlay) {
      this.overlay.unmount();
    }
    this.room.unmount();
    this.overlay = new rooms[name](this);
    this.overlay.mount(this);
    this.refresh();
  }

  //
  // --------------------------------------------------------------------------
  //

  closeOverlay() {
    this.overlay.unmount();
    this.overlay = null;
    this.room.mount();
    this.refresh();
    this._bindHotkeys();
  }

  //
  // --------------------------------------------------------------------------
  //

  get timeString() {
    const timeOfDay = this.timeOfDay;
    let msg = {
      morning: [
        'The silence is oppressive',
        'Not even the wind is stirring',
        'The air is heavy with smoke and the smell of decaying flesh',
        'Even the birds are silent'
      ],

      afternoon: [
        'The air is warm.',
        'It smells of hot asphalt and putrid flesh.',
        'You are sweating heavily.',
        'The smell of dead bodies makes your eyes water.',
        'The heat and stench make your head ache',
        'Your shirt grows wet with persperation'
      ],

      evening: [
        'The sun will set soon.',
        'Darkness fast approaches.',
        'You should seek shelter soon.',
        'Light begins to fade.'
      ],

      night: [
        'Night has fallen.',
        'The night is as black as pitch.',
        'You hear the rumblings of the dead, but cannot see them.',
        'You sense danger fast approaching'
      ]
    }

    if(this.player.hasSophie()) {
      msg.morning = [
        'It\'s a beautiful day!',
        'Sophie sniffs the air with her eyes closed',
        'You and Sophie pee on the same fire hydrant',
        'What a lovely morning!',
        'Birds are singing in the trees all around you',
      ]

      msg.afternoon = [
        'You throw a stick and Sophie fetches it.',
        'You make up a song about how Sophie is cute in time with her tail wags',
        'You race Sophie to the end of the block, she wins',
        'Is this the best day ever?  Probably.'
      ]

      msg.evening = [
        'This is your favorite time of day',
        'You can hear the chirping of crickets',
        'The first few fireflies are coming out',
        'You hear a frog croak in the distance',
        'You walk past blooming honeysuckle and it makes you sneeze',
        'What a beautiful evening',
      ]

      msg.night = [
        'Sophie looks sleepy, but you guys are having fun',
        'You and Sophie take a break, you rest your head on her fur',
        'You look up and realize without electricity the night is stunning',
        'You and Sophie spend some time inventing constellations'
      ]
    }

    const msgs = msg[timeOfDay]
    return msgs[Math.floor(msgs.length*Math.random())]
  }

  //
  // --------------------------------------------------------------------------
  //

  get maxPlayerHp() {
    let zkills = 0;
    if(this.player) zkills = zkills || this.player.zkills;
    return 10 + Math.floor(zkills / 10);
  }

  //
  // --------------------------------------------------------------------------
  //

  removeFood(skip) {
      for(var key in this.inventory) {
        var item = this.inventory[key];
        if(item.food && item.count > 0 && item.id != skip) {
          item.count = item.count - 1;
          return item.name;
        }
      }
      return false;
  }

  //
  // --------------------------------------------------------------------------
  //

  removeItem(id) {
    if(this.hasItem(id)) {
      this.inventory[id].count -= 1
    }
  }
  

  //
  // --------------------------------------------------------------------------
  //

  hasItem(id) {
    return this.inventory[id] && this.inventory[id].count > 0;
  }
  

  //
  // private methods //////////////////////////////////////////////////////////
  //

  _update() {
    setTimeout(this._update, 15);
  }

  //
  // --------------------------------------------------------------------------
  //

  _bindHotkeys() {
    hotkeys('shift+/', ()=>{
      this.openOverlay('OverlayHelp');
      return true;
    });
    hotkeys('shift+i', ()=>{
      this.openOverlay('OverlayInventory');
      return true;
    })
  }
  
}
