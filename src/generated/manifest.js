
//
// (c) GameJammers 2021
// http://www.jamming.games
//

// WARNING!  This file is generated, edits will be overwritten

const Manifest = {
    sophie: require('@generated/sophie.js'),
    sewercreature: require('@generated/sewercreature.js'),
    sewer1: require('@generated/sewer1.js'),
    street8: require('@generated/street8.js'),
    rat: require('@generated/rat.js'),
    forestwin: require('@generated/forestwin.js'),
    street9: require('@generated/street9.js'),
    bedroom: require('@generated/bedroom.js'),
    forest2: require('@generated/forest2.js'),
    street6: require('@generated/street6.js'),
    sewer2: require('@generated/sewer2.js'),
    forest3: require('@generated/forest3.js'),
    castle: require('@generated/castle.js'),
    zombiehorde: require('@generated/zombiehorde.js'),
    dawn: require('@generated/dawn.js'),
    street2: require('@generated/street2.js'),
    street1: require('@generated/street1.js'),
    zombie: require('@generated/zombie.js'),
    forestwin: require('@generated/forestwin.js'),
    fire: require('@generated/fire.js'),
    street5: require('@generated/street5.js'),
    street4: require('@generated/street4.js'),
    hallway: require('@generated/hallway.js'),
    fatandhappy: require('@generated/fatandhappy.js'),
    police: require('@generated/police.js'),
    forest: require('@generated/forest.js'),
    bathroom: require('@generated/bathroom.js'),
    kitchen: require('@generated/kitchen.js'),
    ruins: require('@generated/ruins.js'),
    zombiepack: require('@generated/zombiepack.js'),
    street3: require('@generated/street3.js'),
    cupboard: require('@generated/cupboard.js'),
    sewer3: require('@generated/sewer3.js'),
    street7: require('@generated/street7.js'),
    forest4: require('@generated/forest4.js')
};

export default Manifest;
