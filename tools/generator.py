#
# (c) GameJammers 2021
# http://www.jamming.games
#

import math;
import os;
from PIL import Image, ImageColor;

# CONFIG ######################################################################
# mapped from light to dark
ASCII_CHARS = [ '.', ',', ':', '+', 'o', '*', '&', '#' ] 
CHAR_ASPECT_SKEW = 0.5 #characters are half as tall as they are wide

#
# functions ###################################################################
#

def mag(pixel):
    return math.sqrt(pixel[0] ** 2 + pixel[1] ** 2 + pixel[2] ** 2) / 442

#
# #############################################################################
#

def image_to_glyphs(image, palette, size):
    # resize image
    (owidth, oheight) = image.size
    aspect = oheight / float(owidth)
    new_height = int(aspect * size * CHAR_ASPECT_SKEW)

    image = image.resize((size, new_height))

    # get raw pixel colors and convert those by value (mag) to a corresponding Character
    pixels = list(image.getdata())
    colors = pixels
    if palette:
        colors = [palette[math.floor((len(palette)-1) * mag(pixel))] for pixel in pixels]

    chars = "".join([ASCII_CHARS[math.floor((len(ASCII_CHARS)-1) * mag(pixel))] for pixel in pixels])
    return [{'glyph': i[0], 'color': {'r': i[1][0], 'g': i[1][1], 'b': i[1][2], 'a': i[1][3]/255.0}} for i in zip(chars, colors)]
    
#
# #############################################################################
#

def glyphs_to_html(glyphs, size):
    rows = [glyphs[index: index+size] for index in range(0, len(glyphs), size)]
    final = [''.join([
        '<span style="color:rgb({0},{1},{2},{3})">{4}</span>'.format(
            item['color']['r'], 
            item['color']['g'], 
            item['color']['b'], 
            item['color']['a'], 
            item['glyph']) 
        for item in row]) for row in rows]
    return "<br />\n".join(final)

def glyphs_to_js(name, glyphs):
    js = """
//
// (c) GameJammers 2021
// http://www.jamming.games
//

// WARNING! This is a generated file, any edits will be overwritten!

module.exports = {{
    ascii: require('@generated/{0}.ascii'),
    data: [
        {1}
    ]
}}

"""

    entries = ',\n        '.join(['{{glyph: "{4}", color: {{r: {0}, g: {1}, b: {2}, a: {3}}} }}'.format(
            item['color']['r'], 
            item['color']['g'], 
            item['color']['b'], 
            item['color']['a'], 
            item['glyph']) for item in glyphs])
    return js.format(name, entries)

#
# generate image ##############################################################
#

def generate(input, output, paletteFile, size):
    image = Image.open(input).convert("RGBA");
    
    # palette works fine but i ended up converting the images myself before
    # ASCII-fying them, so i could play with contrast and brightness etc
    palette = None
    if os.path.exists(paletteFile):
        palette = [c[1] for c in Image.open(paletteFile).convert("RGBA").getcolors()]
        palette.sort(key=lambda c: c[0]+c[1]+c[2])

    glyphs = image_to_glyphs(image, palette, size)

    #
    # convert glyphs to html
    #
    name = os.path.basename(output)
    html = glyphs_to_html(glyphs, size)
    js = glyphs_to_js(name, glyphs)

    f = open(output+'.ascii', 'w')
    f.write(html)
    f.close()

    f = open(output+'.js', 'w')
    f.write(js)
    f.close()
