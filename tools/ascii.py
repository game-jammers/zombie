#
# (c) GameJammers 2021
# http://www.jamming.games
#

import click;
import json;
import os;
import generator as ASCII;

#
# click entrypoint ############################################################
#

@click.group()
def cli():
    pass

#
# #############################################################################
#

@cli.command()
@click.argument('input')
@click.argument('output', default='')
@click.option('--palette', default='')
@click.option('--size', default=60, help='Number of characters wide the image should be')
def image(input, output, palette, size):
    """ Process an input image and output a text file containing the ASCII art with the specified SIZE number of columns """
    if not output:
        output = '../src/generated/{0}'.format(os.path.splitext(os.path.basename(input))[0])
    ASCII.generate(input, output, palette, size)

#
# #############################################################################
#

def generateManifest(outdir, files):
    content = """
//
// (c) GameJammers 2021
// http://www.jamming.games
//

// WARNING!  This file is generated, edits will be overwritten

const Manifest = {{
    {0}
}};

export default Manifest;
"""
    entries = ',\n    '.join(["{0}: require('@generated/{0}.js')".format(os.path.splitext(f)[0]) for f in files])
    return content.format(entries);

#
# -----------------------------------------------------------------------------
#

@cli.command()
@click.argument('input', default='../artsource/images')
@click.argument('output', default='../src/generated')
@click.option('--palette', default='')
@click.option('--size', default=100, help='Number of characters wide the image should be')
def condir(input, output, palette, size):
    """ Process an entire directory of images and write them to the output directory """

    outfiles = []
    for( dirpath, dirnames, filenames ) in os.walk(input):
        for filename in filenames:
            if not filename.endswith('Zone.Identifier'):
                inpath = os.path.join(dirpath, filename)
                outfile = os.path.splitext(filename)[0]
                outpath = os.path.join(output, outfile)
                outfiles.append(outfile)
                ASCII.generate(inpath, outpath, palette, size)

        manifest = generateManifest(output, outfiles)
        manfile = os.path.join(output, 'manifest.js')
        f = open(manfile, 'w')
        f.write(manifest)
        f.close()

#
# python main #################################################################
#

if (__name__ == '__main__'):
    cli()
